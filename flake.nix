{
  description = "Something-elm";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    # naersk.url = "github:nix-community/naersk/master";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    { self
    , nixpkgs
    , flake-utils
      # , naersk
    }: flake-utils.lib.eachDefaultSystem
      (system:
      let
        packageName = "something-elm";

        pkgs = nixpkgs.legacyPackages.${system};
        # naersk-lib = pkgs.callPackage naersk { };

        # libs = with pkgs; [
        #   webkitgtk
        #   gtk3
        #   cairo
        #   gdk-pixbuf
        #   glib
        #   dbus
        #   openssl_3
        # ];
      in
      rec {
        # The application we are packaging.
        # Can be packaged in various ways, but we use these builders:
        # https://nixos.org/manual/nixpkgs/stable/#python
        # buildPythonPackage would have been used for libraries.
        # packages.${packageName} = pkgs.python3Packages.buildPythonApplication rec {
        #   name = packageName;
        #   src = ./src;
        #   buildInputs = [
        #     pkgs.websocat
        #     pkgs.watchexec # to rebuild after saving

        #     pkgs.elmPackages.elm
        #     pkgs.elmPackages.elm-format
        #     pkgs.elmPackages.elm-test
        #     pkgs.elmPackages.elm-language-server
        #   ];
        # };

        # The default package in this flake.
        # defaultPackage = self.packages.${system}.${packageName};

        # A development shell for nix develop.
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            # curl
            # wget
            # pkg-config
            # dbus
            # openssl_3
            # glib
            # gtk3
            # libsoup
            # webkitgtk

            # cargo
            # rustc
            # rustfmt
            # rustPackages.clippy


            # pkgs.websocat
            watchexec # to rebuild after saving
            caddy

            elmPackages.elm
            elmPackages.elm-format
            elmPackages.elm-test
            elmPackages.elm-language-server
          ];

          # shellHook = ''
          #   export LD_LIBRARY_PATH=${pkgs.lib.makeLibraryPath libs}:$LD_LIBRARY_PATH
          # '';

          # RUST_SRC_PATH = rustPlatform.rustLibSrc;
        };
      });

}
