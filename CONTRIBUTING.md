# Contributing to voucher-box

This is a short guide on how to develop and/or contribute to voucher-box.

The application is completely front-end, developed with
[elm](https://elm-lang.org/), Dropbox API and styled with CSS.

The elm source code is in `src/`, while the html and css files are in `assets/`:
you will have to build the `src/Main.elm` file in `assets/main.js`, then serve
the `asset/` directory

To develop, you can use `nix develop`, if you like; `elm` is the only thing you
really need, then you just need a way to serve the `asset` directory and open
it with your preferred browser.

This could be a way to monitor changes and do this automatically, using `caddy`
as a server and `watchexec` to rebuild on changes:

```
watchexec -w src -w assets -- elm make src/Main.elm --output=assets/main.js &
cd assets
caddy file-server --listen :8000 --debug --access-log
```

Building be like

```
elm make src/Main.elm --output=assets/main.js
```

and deploying be like

```
cp -r assets /your/triplew/root/
```

If you have changes, feel free to open a PR: eventually I'll reply :)
