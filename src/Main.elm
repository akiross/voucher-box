module Main exposing (Model, Msg(..), init, main, subscriptions, suite, update, view)

import Browser
import Browser.Navigation as Nav
import Code128
import Expect
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as JD exposing (Decoder)
import Json.Encode as Encode
import Parser as P exposing ((|.), (|=))
import Round
import Set
import Test exposing (Test, describe, test, todo)
import Url exposing (Url)
import Url.Parser exposing ((</>), (<?>), parse, query)
import Url.Parser.Query as Query



-- Configuration


developmentMode : Bool
developmentMode =
    False


clientId : String
clientId =
    "n6gyunio4nxr7ey"


stylesheetPath : Maybe String
stylesheetPath =
    if developmentMode then
        Just "../assets/style.css"

    else
        Nothing


appUrl : String
appUrl =
    if developmentMode then
        "http://localhost:8000/src/Main.elm"

    else
        "https://akiross.codeberg.page/voucher-box/"



-- MAIN


main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }



-- MODEL


type alias Token =
    String


type alias FileName =
    String


type State
    = Failure String
    | WaitingAuth
    | Authorized Token
    | Success Token String
    | Listed Token FileList -- When list of files is displayed, no cards are flipped
    | Selected Token FileList FileName -- When one card is flipped
    | Downloaded Token FileList FileName String -- When the flipped card is loaded
    | Prompted Token FileList FileName -- When one card is being used


type alias Model =
    { message : String, state : State, key : Nav.Key, amountInput : String }


suite : Test
suite =
    let
        -- This url is used as a default if a Maybe Url is needed
        fail =
            { protocol = Url.Https
            , host = "test.data.error"
            , port_ = Nothing
            , path = "/"
            , query = Nothing
            , fragment = Nothing
            }

        -- This is a URL built by hand
        composedUrl =
            { protocol = Url.Http
            , host = "localhost"
            , port_ = Just 8000
            , path = "/src/Main.elm"
            , query = Nothing
            , fragment = Just "foo=bar"
            }

        convertedUrl =
            { fragUrlFromStr | query = fragUrlFromStr.fragment, fragment = Nothing }

        -- An URL with a query string in fragment
        fragUrlFromStr =
            Maybe.withDefault fail <| Url.fromString "http://localhost:8000/src/Main.elm#foo=bar"

        -- An URL with query string
        queryUrlFromStr =
            Maybe.withDefault fail <| Url.fromString "http://localhost:8000/src/Main.elm?foo=bar"

        queryUrlFromStrProd =
            Maybe.withDefault fail <| Url.fromString "https://akiross.codeberg.page/voucher-box/?foo=bar"
    in
    describe "Testing stuff"
        [ test
            "URL from struct same as url from string"
            (\_ -> Expect.equal composedUrl fragUrlFromStr)
        , test
            "Can convert fragment to query URL"
            (\_ -> Expect.equal convertedUrl queryUrlFromStr)

        -- , test "Can parse http://localhost:8000/src/Main.elm#access_token=ciccia"
        --    (\_ -> Expect.equal (parseAccessToken fragUrlFromStr) (Just "foo"))
        , test
            "Can parse query string devel"
            (\_ ->
                Expect.equal
                    (parse
                        (Url.Parser.s "src" </> Url.Parser.s "Main.elm" <?> Query.string "foo")
                        queryUrlFromStr
                    )
                    (Just (Just "bar"))
            )
        , test
            "Can parse query string prod"
            (\_ ->
                Expect.equal
                    (parse
                        (Url.Parser.s "voucher-box" <?> Query.string "foo")
                        queryUrlFromStrProd
                    )
                    (Just (Just "bar"))
            )
        , test "Float subtraction rounding is fixed"
            (\_ ->
                Expect.notEqual (computeNewAmount 8.89 3.33) (String.fromFloat (8.89 - 3.33))
            )
        ]


parseAccessToken : Url -> Maybe String
parseAccessToken url =
    let
        -- Dropbox will redirect using fragment: #access_token=foo&bar=baz&etc
        -- but Elm has query string parser, so we have to convert
        newUrl =
            { url | query = url.fragment, fragment = Nothing }

        -- This prefix is used in the reactor
        -- TODO what prefix is used in production? gotta test with a reverse proxy
        -- on some port using index.html...
        -- TODO oneOf might be used here to make it work in both cases
        prefix =
            -- /src/Main.elm?access_token=XYZ
            if developmentMode then
                Url.Parser.s "src" </> Url.Parser.s "Main.elm"

            else
                Url.Parser.s "voucher-box"
    in
    Maybe.withDefault Nothing
        (parse
            (prefix <?> Query.string "access_token")
            newUrl
        )


init : () -> Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    case parseAccessToken url of
        Just token ->
            ( { message = "", state = Authorized token, key = key, amountInput = "" }
            , Http.request
                { method = "POST"
                , headers =
                    [ Http.header "Authorization" ("Bearer " ++ token)

                    -- , Http.header "Content-Type" "application/json"
                    ]
                , url = "https://api.dropboxapi.com/2/files/list_folder"
                , body =
                    let
                        jsonData =
                            Encode.object [ ( "path", Encode.string "" ) ]
                    in
                    Http.jsonBody jsonData
                , expect = Http.expectJson GotFileList fileListDecoder
                , timeout = Nothing
                , tracker = Nothing
                }
            )

        Nothing ->
            ( { message = "access_token not found in " ++ Url.toString url
              , state = WaitingAuth
              , key = key
              , amountInput = ""
              }
            , Cmd.none
            )


type alias FileMetadata =
    { name : String
    , path_lower : Maybe String
    , path_display : Maybe String
    , preview_url : Maybe String
    , id : String
    , rev : String
    , size : Int
    }


type alias FileList =
    { entries : List FileMetadata, cursor : String, has_more : Bool }


fileMetadataDecoder : Decoder FileMetadata
fileMetadataDecoder =
    JD.map7 FileMetadata
        (JD.field "name" JD.string)
        (JD.maybe (JD.field "path_lower" JD.string))
        (JD.maybe (JD.field "path_display" JD.string))
        (JD.maybe (JD.field "preview_url" JD.string))
        (JD.field "id" JD.string)
        (JD.field "rev" JD.string)
        (JD.field "size" JD.int)


fileListDecoder : Decoder FileList
fileListDecoder =
    JD.map3 FileList
        (JD.field "entries" (JD.list fileMetadataDecoder))
        (JD.field "cursor" JD.string)
        (JD.field "has_more" JD.bool)


fileDownloadDecoder : Decoder String
fileDownloadDecoder =
    JD.string



-- Compute new amount by subtraction and rounding, then get it back as string


computeNewAmount : Float -> Float -> String
computeNewAmount total amount =
    Round.round 2 (total - amount)


type alias FileId =
    String


type Msg
    = GotFileList (Result Http.Error FileList)
    | UrlChanged Url
    | LinkClicked Browser.UrlRequest
    | ConfirmRename FileId String Voucher
    | PromptRename
    | CancelPrompt
    | Hide
    | InputUpdate String
    | FileMoved FileName (Result Http.Error FileMetadata)
    | FileDownloaded FileName (Result Http.Error String)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UrlChanged url ->
            case url.fragment of
                Just fragment ->
                    case parseFileName fragment of
                        Ok voucher ->
                            case model.state of
                                Listed token files ->
                                    ( { model
                                        | message = "Downloading card " ++ fragment
                                        , state = Selected token files fragment
                                        , amountInput = voucher.amount
                                      }
                                    , Http.request
                                        { method = "POST"
                                        , headers =
                                            [ Http.header "Authorization" ("Bearer " ++ token)
                                            , Http.header "Dropbox-API-Arg" (Encode.encode 0 <| Encode.object [ ( "path", Encode.string ("/" ++ fragment) ) ])
                                            ]
                                        , url = "https://content.dropboxapi.com/2/files/download"
                                        , body = Http.emptyBody -- Http.jsonBody (Encode.object [ ( "path", Encode.string fragment ) ])
                                        , expect = Http.expectString (FileDownloaded fragment)
                                        , timeout = Nothing
                                        , tracker = Nothing
                                        }
                                    )

                                Selected token files _ ->
                                    ( { model
                                        | message = "Downloading card" ++ fragment
                                        , state = Selected token files fragment
                                        , amountInput = voucher.amount
                                      }
                                    , Http.request
                                        { method = "POST"
                                        , headers =
                                            [ Http.header "Authorization" ("Bearer " ++ token)
                                            , Http.header "Dropbox-API-Arg" (Encode.encode 0 <| Encode.object [ ( "path", Encode.string ("/" ++ fragment) ) ])
                                            ]
                                        , url = "https://content.dropboxapi.com/2/files/download"
                                        , body = Http.emptyBody -- Http.jsonBody (Encode.object [ ( "path", Encode.string fileName ) ])
                                        , expect = Http.expectString (FileDownloaded fragment)
                                        , timeout = Nothing
                                        , tracker = Nothing
                                        }
                                    )

                                Downloaded token files _ _ ->
                                    ( { model
                                        | message = "Downloading card" ++ fragment
                                        , state = Selected token files fragment
                                        , amountInput = voucher.amount
                                      }
                                    , Http.request
                                        { method = "POST"
                                        , headers =
                                            [ Http.header "Authorization" ("Bearer " ++ token)
                                            , Http.header "Dropbox-API-Arg" (Encode.encode 0 <| Encode.object [ ( "path", Encode.string ("/" ++ fragment) ) ])
                                            ]
                                        , url = "https://content.dropboxapi.com/2/files/download"
                                        , body = Http.emptyBody -- Http.jsonBody (Encode.object [ ( "path", Encode.string fileName ) ])
                                        , expect = Http.expectString (FileDownloaded fragment)
                                        , timeout = Nothing
                                        , tracker = Nothing
                                        }
                                    )

                                _ ->
                                    ( { model
                                        | message = "Url changed, not taking action"
                                      }
                                    , Cmd.none
                                    )

                        Err e ->
                            ( { model
                                | message = "Cannot parse file name: " ++ e
                              }
                            , Cmd.none
                            )

                Nothing ->
                    ( { model | message = "Url changed, but no fragment, doing nothing" }, Cmd.none )

        LinkClicked url ->
            case url of
                Browser.Internal link ->
                    -- Do nothing for internal links
                    ( { model | message = "Internal" }
                      -- The pushUrl*2+back trick below won't work; switching in Elm
                    , Nav.pushUrl model.key (Url.toString link)
                      -- Cmd.batch
                      --   -- Updating the url with pushUrl (JS's pushState) will not update the
                      --   -- css :target property, while forward/back will. This is a workaround
                      --   -- see here: https://github.com/whatwg/html/issues/639#issuecomment-1030914480
                      --   [ Nav.pushUrl model.key (Url.toString link)
                      --   , Nav.pushUrl model.key ""
                      --   , Nav.back model.key 1
                      --   ]
                    )

                Browser.External href ->
                    -- Follow external links
                    ( { model | message = "External" }, Nav.load href )

        GotFileList (Err (Http.BadBody m)) ->
            ( { model | message = "Cannot decode file list: " ++ m }, Cmd.none )

        GotFileList (Err (Http.BadStatus n)) ->
            ( { model | message = "Got status error: " ++ String.fromInt n }, Cmd.none )

        GotFileList (Err _) ->
            ( { model | message = "Got another error" }, Cmd.none )

        GotFileList (Ok fl) ->
            case model.state of
                Authorized token ->
                    let
                        entryKey e =
                            case parseFileName e.name of
                                Ok voucher ->
                                    -- Group by service for searching ease
                                    -- Then place status and expiry before amount
                                    voucher.service ++ String.padLeft 6 '0' voucher.amount ++ voucher.status ++ voucher.expiry

                                Err _ ->
                                    e.name

                        sortedEntries =
                            List.sortBy entryKey fl.entries

                        sfl =
                            { fl | entries = sortedEntries }
                    in
                    ( { model | message = "Decoded file list", state = Listed token sfl }, Cmd.none )

                _ ->
                    ( { model | message = "Weird state when GotFileList? TODO" }, Cmd.none )

        ConfirmRename fid fileName originalVoucher ->
            case model.state of
                -- Listed token _ ->
                Prompted token fl fileName_ ->
                    case parseFileName fileName_ of
                        Ok voucher ->
                            case String.toFloat voucher.amount of
                                Just amount ->
                                    case String.toFloat model.amountInput of
                                        Just used ->
                                            let
                                                newVoucher =
                                                    { voucher | amount = computeNewAmount amount used }

                                                newFileName =
                                                    -- The path must be in the root
                                                    "/" ++ voucherTemplate newVoucher
                                            in
                                            ( { model
                                                | message = "Confirm rename " ++ newFileName
                                                , state = Selected token fl fileName
                                                , amountInput = voucher.amount
                                              }
                                            , Http.request
                                                { method = "POST"
                                                , headers = [ Http.header "Authorization" ("Bearer " ++ token) ]
                                                , url = "https://api.dropboxapi.com/2/files/move"
                                                , body =
                                                    let
                                                        jsonData =
                                                            Encode.object
                                                                [ ( "from_path", Encode.string fid )
                                                                , ( "to_path", Encode.string newFileName )
                                                                ]
                                                    in
                                                    Http.jsonBody jsonData
                                                , expect = Http.expectJson (FileMoved fileName) fileMetadataDecoder -- GotFileList fileListDecoder
                                                , timeout = Nothing
                                                , tracker = Nothing
                                                }
                                            )

                                        Nothing ->
                                            ( { model | message = "Invalid input amount" }, Cmd.none )

                                Nothing ->
                                    ( { model | message = "Invalid voucher amount" }, Cmd.none )

                        Err e ->
                            ( { model | message = "Cannot parse file name: " ++ e }, Cmd.none )

                _ ->
                    ( { model | message = "Confirming rename from weird state" }, Cmd.none )

        PromptRename ->
            case model.state of
                -- Item was not downloaded yet
                Selected tok fl s ->
                    ( { model
                        | message = "Showing prompt for file"
                        , state = Prompted tok fl s
                      }
                    , Cmd.none
                    )

                -- Item was downloaded
                Downloaded tok fl s _ ->
                    ( { model
                        | message = "Showing prompt for file"
                        , state = Prompted tok fl s
                      }
                    , Cmd.none
                    )

                _ ->
                    ( { model | message = "Showing prompt from weird state" }, Cmd.none )

        Hide ->
            case model.state of
                Selected tok fl s ->
                    ( { model
                        | message = "Hiding card"
                        , state = Listed tok fl
                      }
                    , Cmd.none
                    )

                Downloaded tok fl s _ ->
                    ( { model
                        | message = "Hiding card"
                        , state = Listed tok fl
                      }
                    , Cmd.none
                    )

                _ ->
                    ( { model | message = "Hiding card from weird state" }, Cmd.none )

        CancelPrompt ->
            case model.state of
                Prompted tok fl fileName ->
                    case parseFileName fileName of
                        Ok voucher ->
                            ( { model
                                | message = "Hiding prompt"
                                , state = Selected tok fl fileName
                                , amountInput = voucher.amount
                              }
                            , Cmd.none
                            )

                        Err e ->
                            ( { model | message = "Cannot parse file name: " ++ e }, Cmd.none )

                _ ->
                    ( { model | message = "Hiding prompt from weird state" }, Cmd.none )

        InputUpdate v ->
            ( { model | amountInput = v }, Cmd.none )

        FileMoved fn (Err _) ->
            ( { model | message = "Cannot rename file " ++ fn }, Cmd.none )

        FileMoved origName (Ok fm) ->
            let
                -- This function replaces a single entry in a list of files metadata
                replaceEntry entries oldName newName =
                    List.map
                        (\m ->
                            if m.name == oldName then
                                { m | name = newName }

                            else
                                m
                        )
                        entries
            in
            case model.state of
                Selected tok fileList _ ->
                    ( { model
                        | message = "File renamed " ++ fm.name
                        , state =
                            Listed tok
                                { entries = replaceEntry fileList.entries origName fm.name
                                , cursor = ""
                                , has_more = False
                                }
                      }
                    , Cmd.none
                    )

                _ ->
                    ( { model | message = "Invalid state after renaming" }, Cmd.none )

        FileDownloaded fileName result ->
            case result of
                Ok content ->
                    case model.state of
                        Listed tok fileList ->
                            ( { model
                                | message = "Downloaded content " ++ content
                                , state = Downloaded tok fileList fileName content
                              }
                            , Cmd.none
                            )

                        Selected tok fileList _ ->
                            ( { model
                                | message = "Downloaded content " ++ content
                                , state = Downloaded tok fileList fileName content
                              }
                            , Cmd.none
                            )

                        _ ->
                            ( { model | message = "Downloaded, but invalid state" }, Cmd.none )

                Err (Http.BadUrl err) ->
                    ( { model | message = "Cannot download: Bad Url" ++ err }, Cmd.none )

                Err Http.Timeout ->
                    ( { model | message = "Cannot download: timeout" }, Cmd.none )

                Err (Http.BadBody err) ->
                    ( { model | message = "Cannot download: Bad  Body" ++ err }, Cmd.none )

                Err Http.NetworkError ->
                    ( { model | message = "Cannot download: network error" }, Cmd.none )

                Err (Http.BadStatus err) ->
                    ( { model | message = "Cannot download: Bad Status " ++ String.fromInt err }, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


type alias Voucher =
    { service : String
    , seq : String
    , status : String
    , expiry : String
    , amount : String
    , ext : String
    }


voucherTemplate : Voucher -> String
voucherTemplate v =
    v.service ++ "_" ++ v.seq ++ "_" ++ v.status ++ "_" ++ v.expiry ++ "_" ++ v.amount ++ "eur." ++ v.ext


deadEndToString : P.DeadEnd -> String
deadEndToString de =
    let
        position =
            "[" ++ String.fromInt de.row ++ ", " ++ String.fromInt de.col ++ "]: "

        message =
            case de.problem of
                P.Expecting s ->
                    "Expecting '" ++ s ++ "'"

                P.ExpectingInt ->
                    "Expecting int"

                P.ExpectingHex ->
                    "Expecting hex"

                P.ExpectingOctal ->
                    "Expecting octal"

                P.ExpectingBinary ->
                    "Expecting binary"

                P.ExpectingFloat ->
                    "Expecting float"

                P.ExpectingNumber ->
                    "Expecting number"

                P.ExpectingVariable ->
                    "Expecting variable"

                P.ExpectingSymbol s ->
                    "Expecting symbol '" ++ s ++ "'"

                P.ExpectingKeyword s ->
                    "Expecting keyword '" ++ s ++ "'"

                P.ExpectingEnd ->
                    "Expecting end"

                P.UnexpectedChar ->
                    "Unexpected char"

                P.Problem s ->
                    "Problem: '" ++ s ++ "'"

                P.BadRepeat ->
                    "Bad repeat"
    in
    position ++ message


formatParsingError : String -> List P.DeadEnd -> String
formatParsingError text deadEnds =
    "Could not parse string '" ++ text ++ "': " ++ String.join " " (List.map deadEndToString deadEnds)


parseFileName : String -> Result String Voucher
parseFileName name =
    let
        word =
            P.variable
                { start = \c -> c /= '_'
                , inner = \c -> c /= '_'
                , reserved = Set.empty
                }

        anything =
            P.variable
                { start = \_ -> True
                , inner = \_ -> True
                , reserved = Set.empty
                }

        value =
            P.variable
                { start = Char.isDigit
                , inner = \c -> Char.isDigit c || c == '.'
                , reserved = Set.empty
                }

        date =
            P.variable
                { start = Char.isDigit
                , inner = \c -> Char.isDigit c || c == '-'
                , reserved = Set.empty
                }

        voucher =
            P.succeed Voucher
                |= word
                |. P.symbol "_"
                |= word
                |. P.symbol "_"
                |= word
                |. P.symbol "_"
                |= date
                |. P.symbol "_"
                |= value
                |. P.keyword "eur"
                |. P.symbol "."
                |= anything
    in
    case P.run voucher name of
        Ok vp ->
            Ok vp

        Err e ->
            Err (formatParsingError name e)



-- VIEW


renderCard : Maybe { name : String, content : String } -> FileMetadata -> Maybe (Html Msg)
renderCard flipped file =
    let
        classesSelected =
            Maybe.withDefault [ class "invisible" ] <|
                Maybe.map
                    (\f ->
                        if f.name == file.name then
                            []

                        else
                            [ class "invisible" ]
                    )
                    flipped

        classesUnselected =
            Maybe.withDefault [] <|
                Maybe.map
                    (\f ->
                        if f.name /= file.name then
                            []

                        else
                            [ class "invisible" ]
                    )
                    flipped
    in
    case parseFileName file.name of
        Ok voucher ->
            case String.toFloat voucher.amount of
                Just amount ->
                    if amount <= 0 || voucher.status == "empty" then
                        -- Empty
                        -- Just (li [] [ text "Empty" ])
                        Nothing

                    else
                        let
                            image =
                                case voucher.ext of
                                    "jpg" ->
                                        img [ class "p-img", attribute "data-dbx-src" file.id ] []

                                    "jpeg" ->
                                        img [ class "p-img", attribute "data-dbx-src" file.id ] []

                                    "png" ->
                                        img [ class "p-img", attribute "data-dbx-src" file.id ] []

                                    "c128" ->
                                        case flipped of
                                            Just flip ->
                                                case Code128.fromString flip.content of
                                                    Err _ ->
                                                        a [ class "p-download", attribute "data-dbx-href" file.id ] [ text "Download (C128 fail)" ]

                                                    Ok rendered ->
                                                        div [ class "p-img" ]
                                                            [ Code128.widthsToSvg 30 rendered
                                                            , div [ class "centered" ] [ text flip.content ]
                                                            ]

                                            Nothing ->
                                                a [ class "p-download", attribute "data-dbx-href" file.id ] [ text "Download (C128 fail)" ]

                                    _ ->
                                        a [ class "p-download", attribute "data-dbx-href" file.id ] [ text "Download (Unknown)" ]
                        in
                        Just
                            (li
                                [ class "file-card"
                                , id file.name
                                , attribute "data-status" voucher.status
                                , attribute "data-amount" (String.fromFloat amount)
                                ]
                                [ div
                                    (classesUnselected ++ [ class "flip-front" ])
                                    [ span [ class "p-service" ] [ text voucher.service ]
                                    , span [ class "p-expiry" ] [ text voucher.expiry ]
                                    , span [ class "p-amount" ] [ text (String.fromFloat amount ++ " €") ]

                                    --, span [ class "p-ext" ] [ text voucher.ext ]
                                    , a [ class "p-flip", href ("#" ++ file.name) ] [ text "Show" ]
                                    ]
                                , div
                                    (classesSelected ++ [ class "flip-back" ])
                                    [ -- , span [] [ text (String.fromFloat voucher.amount) ]
                                      image
                                    , div [] [ text "" ]
                                    , button
                                        [ class "p-use", onClick PromptRename ]
                                        [ text "Use" ]
                                    , button
                                        [ class "p-hide", onClick Hide ]
                                        [ text "Hide" ]
                                    ]
                                ]
                            )

                Nothing ->
                    -- Invalid amount
                    -- Just (li [] [ text "Ignored invalid amount" ])
                    Nothing

        Err e ->
            -- Error, failed parsing
            -- Just (li [] [ text ("Ignored because pirates: " ++ e) ])
            Nothing


view : Model -> Browser.Document Msg
view model =
    { title = "VoucherBox"
    , body =
        (case stylesheetPath of
            Just stylesheet ->
                [ Html.node "link" [ Html.Attributes.rel "stylesheet", Html.Attributes.href stylesheet ] [] ]

            Nothing ->
                []
        )
            ++ [ text model.message
               , case model.state of
                    Failure s ->
                        text ("I was unable to load your book: " ++ s)

                    WaitingAuth ->
                        let
                            -- "https://dropbox.com/oauth2/authorize?response_type=token&client_id=n6gyunio4nxr7ey&redirect_uri=
                            authUrl =
                                "https://www.dropbox.com/oauth2/authorize"
                                    ++ "?client_id="
                                    ++ clientId
                                    ++ "&response_type=token"
                                    ++ "&redirect_uri="
                                    ++ appUrl
                        in
                        a [ class "auth-button", href authUrl ] [ text "Authorize" ]

                    Authorized token ->
                        text "Loading..."

                    Success _ fullText ->
                        pre [] [ text fullText ]

                    Listed _ fl ->
                        div []
                            [ h1 [] [ text "My VoucherBox" ]
                            , div []
                                [ ul [ class "file-cards" ] (List.filterMap (renderCard Nothing) fl.entries)
                                ]
                            ]

                    Selected _ fl name ->
                        div []
                            [ h1 [] [ text "My VoucherBox" ]
                            , div []
                                [ ul [ class "file-cards" ] (List.filterMap (renderCard Nothing) fl.entries)
                                ]
                            ]

                    Downloaded _ fl name content ->
                        div []
                            [ h1 [] [ text "My VoucherBox" ]
                            , div []
                                [ ul [ class "file-cards" ] (List.filterMap (renderCard (Just { name = name, content = content })) fl.entries)
                                ]
                            ]

                    Prompted _ fl name ->
                        let
                            filterFileByName =
                                \f ->
                                    if f.name == name then
                                        Just f

                                    else
                                        Nothing
                        in
                        case List.filterMap filterFileByName fl.entries of
                            file :: [] ->
                                case parseFileName file.name of
                                    Ok voucher ->
                                        div [ class "input-modal" ]
                                            [ text "Consumed amount"
                                            , div [ class "input-row" ]
                                                [ input
                                                    [ required True
                                                    , value model.amountInput
                                                    , type_ "number" -- pattern "-?[0-9]+"
                                                    , step "0.01" -- accuracy down to cents
                                                    , onInput InputUpdate
                                                    ]
                                                    []
                                                ]
                                            , div [ class "button-row" ]
                                                [ button [ onClick CancelPrompt ] [ text "Cancel" ]
                                                , button [ onClick (ConfirmRename file.id file.name voucher) ] [ text "Update" ]
                                                ]
                                            ]

                                    Err _ ->
                                        text "Whoops"

                            [] ->
                                text "Error no elements in list"

                            _ ->
                                text "Something else in the list"
               ]
    }
