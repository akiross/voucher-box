# voucher-box

This is a simple application that can be used to manipulate dropbox files in a
way that can be used to manage vouchers/gift cards, such as the ones that might
be used when shopping.

## Purpose

It covers a simple use case: a person (typically, me) has a few voucher that can
be used to shop, the person wants to have an archive of those vouchers in a
convenient way so that they can be used when shopping.

The assumption is that vouchers can be somehow encoded into images or code 128
barcodes. This is not necessarily true, but makes sense most of the time,
so I'm not planning to add support to other features shortly.

### Example

Here's an example: someone give you a gift card for a famous Italian retail
store chain (I've tested this with [Unes](https://www.unes.it/) and
[Esselunga](https://en.wikipedia.org/wiki/Esselunga) gift cards), but this seems
to be a fairly common gift lately, so you end up having 10 gift cards after
holiday season.

Those gift cards are usually representing money and each of them can be used in
more than one transaction, so you might want to spend a 20 euros gift card in
2 transactions of 10 euros each.

You don't want to keep all of them stashed somewhere in your house or wallet,
since you can either lose them, forget them at home or just be annoyed by the
fact that you have to remember how much credit you have left on each of them.

Enter voucher-box!

## How to use

To prepare the vouchers:

1. Open a browser and go to https://akiross.codeberg.page/voucher-box/
2. Authorize the app and log in with your Dropbox account if necessary; the app
   should require permissions only to read and write files in its own folder;
3. Place your voucher files in the voucherbox directory (likely
   `Dropbox/Apps/VoucherBox`): each file can be either an image or a code to be
     rendered as a [Code 128 barcode](https://it.wikipedia.org/wiki/Code_128)
     and will have to follow a certain naming convention (see below)

You can use the files in `examples/` if you want some files to test with.

To use the app:

1. Open a browser and go to https://akiross.codeberg.page/voucher-box/
2. Authorize the app;
3. Select the voucher you want to use, click `Show`;
   - the listed vouchers are the ones that have positive `<amount>` or with a
     `<status>` that is not `empty`;
4. Scan the code or the image as needed;
5. If any credit is used, press `Use`, input the used amount (defaults to the
   available credit) and `Confirm`;
   - this will trigger a rename of the file: the used amount will be scaled from
     the `<amount>` value in the file name and the name will be changed
     accordingly;
   - if the amount becomes zero (or less), the voucher is removed from the list;
6. Use the vouchers as much as you like!

## File naming convention

```
<name>_<seq>_<status>_<yyyy-mm-dd>_<amount>eur.<ext>
```

- `<name>` is a string for your own convenience (e.g. `Esselunga`);
- `<seq>` is a string for your own convenience (e.g. `01`);
- `<status>` is a string that can be `empty`, `used` or `full`: when the
  string is `empty`, it is not shown in the list; the card will then be
  rendered in different colors depending on the status (used or full);
- `<yyyy-mm-dd>` is the expiry date of the voucher;
- `<amount>` is a number (e.g. `123.456`) with the residual credit;
- `<ext>` is `jpg`, `jpeg`, `png` or `c128`.

**None** of these segments can contain underscores (`_`), as they are used as
separators.

## FAQ

Q: Why Dropbox?
A: Dropbox is used because it's reasonably diffused and allows for easy sharing
   of directories among different accounts; this allows to share the vouchers
   when multiple people have to access them, such as in a typical household.
   Note that voucher-box does is not robust with concurrent usage, at the moment
   so it's Ok to share the files, but work on different files at the same time.
